import 'dart:io';
 
void main(List<String> arguments) {
  print("Enter sentence:");
  String? words = stdin.readLineSync()!;
  String lower_words = words.toLowerCase();
  countWords(lower_words);
}

void countWords(String val){
  List arr = val.replaceAll(r".", " ")
  .replaceAll(r"?", "")
  .replaceAll(r"!", "")
  .replaceAll(r",", "")
  .replaceAll(new RegExp(r"[0-9]"),"")
  .replaceAll(r"  ", " ")
  .split(' ');

  List count = new List<int>.filled(arr.length, 1, growable: true);

  for(int i = 0; i <= arr.length-2; i++){
    for(int j = i+1; j <= arr.length-1; j++){
      var result = arr[i].compareTo(arr[j]);
      if(result == 0){
        count[i]++;
        arr.removeAt(j);
        count.removeAt(j);
      }
    }
  }
  for(int i = 0; i <= arr.length-1; i++){
    print(arr[i] + ": " + count[i].toString());
  }  
}