import 'dart:io';
 
void main(List<String> arguments) {
  while(true){
    print("Menu");
    print("Select the choice you want to perform : \n"
        "1. ADD\n"
        "2. SUBTRACT\n"
        "3. MULTIPLY\n"
        "4. DIVIDE\n"
        "5. EXIT\n");
    print("Choice you want to enter :");
    int? menu = int.parse(stdin.readLineSync()!);
    if(menu == 5) {
      break;
    }

    print("Enter the value for x :");
    double? x = double.parse(stdin.readLineSync()!);
    print("Enter the value for y :");
    double? y = double.parse(stdin.readLineSync()!);
    print("Sum of the two numbers is :");
    calMenu(menu, x, y);
    print("");
  }
}

void calMenu(int menu, double x, double y){
  switch (menu) {
    case 1:
      {
        print(add(x,y));
      }
      break;
    case 2:
      {
        print(sub(x,y));
      }
      break;
    case 3:
      {
        print(mul(x,y));
      }
      break;
    case 4:
      {
        print(divide(x,y));
      }
      break;
  }
}

double? divide(double x, double y) {
  return x/y;
}

double? mul(double x, double y) {
  return x*y;
}

double? sub(double x, double y) {
  return x-y;
}

double add(double x, double y) {
  return x+y;
}
